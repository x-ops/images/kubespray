#!/usr/bin/env bash

set -eu

getImage() {
    local registry repository version
    version=${KUBESPRAY_VERSION}

    if [ -n "${CI:-}" ] && [ -n "${CI_REGISTRY_IMAGE:-}" ]; then
        registry=$(echo "$CI_REGISTRY_IMAGE" | cut -d '/' -f 1)
        repository=$(echo "$CI_REGISTRY_IMAGE" | cut -d '/' -f 2-)
    else
        registry=${IMAGE_REGISTRY:-"registry.gitlab.com"}
        repository=${IMAGE_REPOSITORY:-"x-ops/images/kubespray"}
    fi

    echo "$registry/$repository:$version"
}

printImageInfo() {
    local imageTag imageInfo imageArchitecture imageOs imageSize imageLayers

    imageTag="$1"
    imageInfo=$(docker image inspect "$imageTag")
    imageArchitecture=$(echo "$imageInfo" | jq -r '.[0] | .Architecture')
    imageOs=$(echo "$imageInfo" | jq -r '.[0] | .Os')
    imageSize=$(echo "$imageInfo" | jq -r '.[0] | .Size')
    imageLayers=$(echo "$imageInfo" | jq -r '.[0] | .RootFS.Layers[]' | wc -l)

    echo "OS: $imageOs"
    echo "Arch: $imageArchitecture"
    echo "Size: $imageSize"
    echo "Layers: $imageLayers"
}

if [ $# -lt 2 ]; then
    echo "Usage: $(basename "$0") <Kubespray version> <Python version>" >&2
    exit 1
else
    export KUBESPRAY_VERSION="$1"
    export PYTHON_VERSION="$2"
    shift
    shift
fi

tag=$(getImage)

if [ -n "${CI:-}" ] && [ -n "${GITLAB_CI:-}" ]; then
    docker build . --tag "$tag" \
        --build-arg KUBESPRAY_VERSION \
        --build-arg PYTHON_VERSION \
        --no-cache "$@"

    docker push "$tag"
else
    docker build . --tag "$tag" \
        --build-arg KUBESPRAY_VERSION \
        --build-arg PYTHON_VERSION \
        "$@"

    printImageInfo "$tag"
fi
